using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using RabbitMQ.Client;
using SMETS2.Notifications.In.Api.Interfaces;

namespace SMETS2.Notifications.In.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SMETS2.Notifications.In.Api", Version = "v1" });
                c.EnableAnnotations();
            });

            // TODO: Adjust how the health checks are completed against RabbitMQ
            // Provide extra info (not just "Healthy" or "Unhealthy")
            services
                 .AddHealthChecks()
                 .AddRabbitMQ(
                    mq =>
                    {
                        return new ConnectionFactory();
                    });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SMETS2.Notifications.In.Api v1"));
            app.UseHealthChecks("/hc");
            app.UseHttpsRedirection(); 
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc");
                endpoints.MapControllers();
            });
            // Initialise the Configuration Service on load to ensure it can connect to its dependencies
            app.ApplicationServices.GetService<IMqConfigurationService>();
        }
    }
}
