using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SMETS2.Notifications.In.Api.Interfaces;
using SMETS2.Notifications.In.Api.Services;
using Serilog;
using Serilog.Events;
using System;
using System.IO;
using System.Reflection;
using ULG.MQHelper;
using ULG.MQHelper.Interfaces;

namespace SMETS2.Notifications.In.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location))
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile(
                    $"appsettings.{Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT") ?? "Production"}.json",
                    optional: true)
                .AddEnvironmentVariables()
                .Build();

            // add Serilog logging 
            string serilogFilePath = config.GetSection("SerilogFilePath").Value;
            string serilogFileNamePrefix = DateTime.Now.ToString("yyyyMMdd");
            string serilogFileNameSuffix = config.GetSection("SerilogFileNameSuffix").Value;
            string serilogFullFileName = serilogFilePath + serilogFileNamePrefix + serilogFileNameSuffix;

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Debug)
                .Enrich.FromLogContext()
                .WriteTo.File(serilogFullFileName)
                .CreateLogger();

            try
            {
                Log.Information("Starting Notifications API");
                CreateHostBuilder(args).Build().Run();
                return;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Notifications API unable to start");
                return;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }


        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    var configuration = hostContext.Configuration;

                    services.AddSingleton<IMqConnectionFactory, RabbitFactory>();
                    services.AddSingleton<IMqConfigurationService, MqConfigurationService>();
                    services.AddSingleton<INotificationsService, NotificationsService>();
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseSetting("https_port", "443"); 
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog();
        }
    }
}
