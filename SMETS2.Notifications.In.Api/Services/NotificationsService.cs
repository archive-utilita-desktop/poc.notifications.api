﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;
using SMETS2.Notifications.In.Api.Interfaces;
using SMETS2.Notifications.In.Api.Models;
using System;
using System.Threading.Tasks;
using ULG.MQHelper.Interfaces;

namespace SMETS2.Notifications.In.Api.Services
{
    public class NotificationsService : INotificationsService
    {
        private readonly ILogger<NotificationsService> _logger;
        private readonly IMqConfigurationService _configurationService;
        private IMqConnectionFactory _connectionFactory;

        public NotificationsService(ILogger<NotificationsService> logger,
            IMqConfigurationService configurationService,
            IMqConnectionFactory connectionFactory)
        {
            _logger = logger;
            _configurationService = configurationService;
            _connectionFactory = connectionFactory;
        }

        public async Task PostNotification(NotificationModel model, TenantEnum tenant)
        {
            string message;

            try
            {
                _logger.LogInformation("Deserialising model");

                // Take model from message and serialise it into json
                message = JsonConvert.SerializeObject(model);

                _logger.LogDebug("Model deserialised: {0}", message);
            }
            catch (Exception ex)
            {
                _logger.LogError("Model could not be deserialised: {0}. Exception: {1}", model.ToString(), ex.Message);
                throw ex;
            }

            try
            {
                _logger.LogInformation("Model deserialised, writing to Rabbit queue");

                // Initialise RabbitMQ connection if not already done so by this point
                if (!_configurationService.RabbitMq.IsReady())
                    _configurationService.Initialise();

                // Use Polly to retry the write to RabbitMQ until it succeeds, or if it cannot 
                // write the message after 5 repeated attempts with 1 second delays, 
                // return an error 500 to the controller to report back to the requester
                await Policy
                .Handle<Exception>()
                .WaitAndRetryAsync(
                    5,
                    retryAttempt => TimeSpan.FromSeconds(1),
                    (exception, timespan) =>
                    {
                        _logger.LogWarning("Message was not written to RabbitMQ. "
                            + "Waiting {0} before retrying... Exception: {1}", timespan.ToString(), exception.Message);
                    })
                .ExecuteAsync(async () => await WriteMessageToRabbitMq(tenant, message));
                
            }
            catch (Exception ex)
            {
                _logger.LogError("Message could not be written to RabbitMQ. Message: {0}. Exception: {1}", message, ex.Message);
                throw ex;
            }
        }

        private async Task WriteMessageToRabbitMq(TenantEnum tenant, string message)
        {
            // Queue onto appropriate rabbit queue
            switch (tenant)
            {
                case TenantEnum.Installer:
                    await _connectionFactory.WriteMessageAsync(message, _configurationService.InstallerQueue);
                    _logger.LogInformation("Message for {0} written to {1}", tenant.ToString(), _configurationService.InstallerQueue);
                    break;
                case TenantEnum.Supplier:
                    await _connectionFactory.WriteMessageAsync(message, _configurationService.SupplierQueue);
                    _logger.LogInformation("Message for {0} written to {1}", tenant.ToString(), _configurationService.SupplierQueue);
                    break;
            }
        }
    }
}
