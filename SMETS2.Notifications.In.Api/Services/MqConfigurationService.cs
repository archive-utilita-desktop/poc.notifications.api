﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Polly;
using RabbitMQ.Client.Exceptions;
using SMETS2.Notifications.In.Api.Interfaces;
using System;
using ULG.MQHelper.Interfaces;
using ULG.MQHelper.Models;

namespace SMETS2.Notifications.In.Api.Services
{
    public class MqConfigurationService : IMqConfigurationService
    {
        public IMqConnectionFactory RabbitMq { get; set; }
        public string InstallerQueue { get; set; }
        public string SupplierQueue { get; set; }

        private string _installerExchange;
        private string _supplierExchange;

        private readonly IConfiguration _configuration;
        private readonly ILogger<MqConfigurationService> _logger;

        public MqConfigurationService(IMqConnectionFactory connectionFactory, IConfiguration configuration, ILogger<MqConfigurationService> logger)
        {
            RabbitMq = connectionFactory;
            _configuration = configuration;
            _logger = logger;

            Initialise();
        }

        public void Initialise()
        {
            try
            {
                _logger.LogInformation("Fetching config settings for application");

                // Get Rabbit Connection configuration from appsettings, or use defaults if values aren't specified
                var settings = GetAndSetRabbitConfig();

                // Connect to Rabbit with the provided settings
                InstantiateRabbitConnection(settings);

                _logger.LogInformation("Configuration applied successfully");
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occurred setting up configuration elements of the application. Exception: {0}", ex.Message);

                throw;
            }
        }

        private MqSettingsModel GetAndSetRabbitConfig()
        {
            _installerExchange = _configuration.GetValue(typeof(string), "RabbitMQ:Installer:Exchange", "not.ins.in.dx").ToString();
            InstallerQueue = _configuration.GetValue(typeof(string), "RabbitMQ:Installer:Queue", "not.ins.in.all.q").ToString();

            _supplierExchange = _configuration.GetValue(typeof(string), "RabbitMQ:Supplier:Exchange", "not.sup.in.dx").ToString();
            SupplierQueue = _configuration.GetValue(typeof(string), "RabbitMQ:Supplier:Queue", "not.sup.in.all.q").ToString();

            return new MqSettingsModel
            {
                HostName = _configuration.GetValue(typeof(string), "RabbitMQ:HostName", "localhost").ToString(),
                UserName = _configuration.GetValue(typeof(string), "RabbitMQ:UserName", "guest").ToString(),
                Password = _configuration.GetValue(typeof(string), "RabbitMQ:Password", "guest").ToString(),
                Port = int.Parse(_configuration.GetValue(typeof(string), "RabbitMQ:Port", "5672").ToString())
            };
        }

        private void InstantiateRabbitConnection(MqSettingsModel settings)
        {
            // Initiate connection and register queues in RabbitMQ
            RabbitMq.RegisterConnectionDetails(settings);

            // Use Polly to retry the connection to RabbitMQ until it succeeds, or if it times out 
            // after 5 attempts, throw an exception and shut down the service
            Policy
            .Handle<BrokerUnreachableException>()
            .WaitAndRetry(
                5,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                (exception, timespan) =>
                {
                    _logger.LogWarning("BrokerUnreachableException was thrown while attempting connection to RabbitMQ. "
                        + "Waiting {0} before retrying... Exception: {1}", timespan.ToString(), exception.Message);
                })
            .Execute(() => RabbitMq.CreateConnection());

            RabbitMq.RegisterExchangeAndQueue(_installerExchange, InstallerQueue);
            RabbitMq.RegisterExchangeAndQueue(_supplierExchange, SupplierQueue);
        }
    }
}
