﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SMETS2.Notifications.In.Api.Interfaces;
using SMETS2.Notifications.In.Api.Models;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace SMETS2.Notifications.In.Api.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("api/Notifications/[controller]")]
    public class InstallerController : ControllerBase
    {
        private readonly ILogger<InstallerController> _logger;
        private readonly INotificationsService _notificationsService;

        public InstallerController(ILogger<InstallerController> logger, INotificationsService notificationsService)
        {
            _logger = logger;
            _notificationsService = notificationsService;
        }

        /// <summary>
        /// Endpoint for TMA (or other provider) to post responses or events to Utilita
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(203, "The message was queued")]
        [SwaggerResponse(400, "Request body was invalid")]
        [SwaggerResponse(500, "Something went wrong with the request")]
        public async Task<IActionResult> Post([FromBody] NotificationModel model)
        {
            try
            {
                _logger.LogDebug("Installer's Post Endpoint called");
                await _notificationsService.PostNotification(model, TenantEnum.Installer);
                return StatusCode(203);
            }
            catch (Exception ex)
            {
                _logger.LogError("Exception thrown in Installer Controller: {0}", ex.Message);
                return StatusCode(500);
            }
        }
    }
}
