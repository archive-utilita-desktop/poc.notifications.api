namespace SMETS2.Notifications.In.Api.Models
{
    public class NotificationModel
    {
        public string MessageId { get; set; }
        public string RequestReference { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string[] Refs { get; set; }
    }
}
