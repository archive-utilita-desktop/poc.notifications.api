﻿using SMETS2.Notifications.In.Api.Models;
using System.Threading.Tasks;

namespace SMETS2.Notifications.In.Api.Interfaces
{
    public interface INotificationsService
    {
        /// <summary>
        /// Provide the model and which Tenant the message is for and post the message to RabbitMQ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tenant"></param>
        /// <returns></returns>
        Task PostNotification(NotificationModel model, TenantEnum tenant);
    }
}
