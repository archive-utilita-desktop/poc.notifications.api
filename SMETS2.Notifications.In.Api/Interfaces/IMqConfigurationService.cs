﻿using ULG.MQHelper.Interfaces;

namespace SMETS2.Notifications.In.Api.Interfaces
{
    public interface IMqConfigurationService
    {
        IMqConnectionFactory RabbitMq { get; set; }
        string InstallerQueue { get; set; }
        string SupplierQueue { get; set; }
        
        /// <summary>
        /// Get and initialise all configuration required for running of the application
        /// </summary>
        void Initialise();
    }
}
